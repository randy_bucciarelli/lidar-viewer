./lidar-working/README.txt

* This Repository contains the code and sample data used in processing 
   SCBPS LIDAR data.  The scripts utilize the following:
    - Generic Mapping Tools (GMT) commands to generate and process NETCDF grids 
      and extract elevation profile data along MOP transect lines.
      http://gmt.soest.hawaii.edu/
    - CSHELL and NAWK scripting to control automation

* Directory containing LIDAR data and MOP definitions:
./data/
   20090308/   		LIDAR data containing:
       *bins.grd	NETCDF grid of binned data (2 meter)
       *bins.poly	Geographic (lon,lat) extents of *bins.grd
       *new.grd		NETCDF grid of interpolated data (1 meter)
       *new.poly	Geographic (lon,lat) extents of *new.poly
       *new.xyz		Source ASCII LIDAR data which is extracted beach
   mop_transects.def	MOP Transect definitions for San Diego and Orange Counties
   regions.ll		SCBPS Lidar latitude cells (0.0625 degree tiles) 

* Directory containing CSHELL and Generic Mapping Tools:
./scripts
    lview_binbeach.gmt:		Bin lidar beach data using 2m spaced bins
    lview_extractProfiles.gmt:  Extract binned profile data along MOP lines 
    lview_gridbeach.gmt: 	Generate digital elevation model 
    
